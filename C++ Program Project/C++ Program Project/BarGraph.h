#pragma once
#include "Graph.h"
#include <vector>
class BarGraph :public Graph{
public:
	BarGraph();
	BarGraph(vector<string> name, vector<int> power, bool vertical);
	~BarGraph();

	void draw(vector<string> name, vector<int> power, bool vertical);
	void pointLocator(vector<string> name, vector<int> power, int downlines, int index);
	friend  ostream& operator<<(ostream &out, BarGraph &cPoint);
private:
	vector<string> class_name;
	vector<int> class_power;
	bool class_vertical;
};

