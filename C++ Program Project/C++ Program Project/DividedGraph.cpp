#include "DividedGraph.h"
#include <iostream>
#include <iomanip>
#include <math.h>
#include <vector>
DividedGraph::DividedGraph()
{
}

DividedGraph::DividedGraph(vector<string> name, vector<int> power, bool vertical){
	for (int i = 0; i < name.size(); i++) class_name.push_back(name[i]);
	for (int i = 0; i < power.size(); i++) class_power.push_back(power[i]);
	class_vertical = vertical;
}

DividedGraph::~DividedGraph(){


}

void lineDraw(string string1, string string2, int maxLoop){ //Draw the Divided Bar with string and string
	cout << string1;
	for (int j = 0; j < maxLoop; j++) cout << string2;
	cout << string1 << endl;
}

void lineDraw(string string1, int string2, int maxLoop){	//Draw the Divided Bar with string and INT (INT to indicate which heroes in the bar)
	cout << string1;
	for (int j = 0; j < maxLoop; j++){
		if (j == 3) cout << string2;
		else cout << "_";
	}
	cout << string1 << endl;
}

void DividedGraph::pointLocator(float space[], bool drawline, int& downlines, int index, bool vertical){
	if (vertical == true){
		if (index == 0){
			cout << " ";
			for (int j = 0; j < 7; j++) cout << "_";
			cout << endl;
		}else if (index > 0 && index < 10){
			if (space[downlines] - 1 == 0){
				space[downlines]--;
				downlines++;
				drawline = true;
			}else space[downlines]--;

			if (drawline == false) lineDraw("|"," ",7);
			else if (drawline == true) lineDraw("|", downlines, 7);
			
		}else if (index == 10) lineDraw("|", 5, 7);

		drawline = false;
	}else if (vertical == false){
		if (index == 0){
			cout << " ";
			for (int j = 0; j < 30; j++){
				cout << "_";
			}
			cout << endl;
		}else if (index > 0 && index < 5){
			downlines = 0;
			int tempSpace[5];
			for (int j = 0; j < 5; j++){
				tempSpace[j] = space[j];
			}
			cout << "|";
			for (int j = 0; j < 30; j++){
				if (tempSpace[downlines] - 1 == 0){
					drawline = true;
					tempSpace[downlines]--;
					downlines++;
				}else tempSpace[downlines]--;

				if (drawline == true && downlines != 5){
					if (index == 3)cout << downlines;
					else cout << "|";
				}else cout << " ";
				
				drawline = false;
			}
			if (index == 3) cout << 5 << endl;
			else cout << "|" << endl;

		}else if (index == 5){
			downlines = 0;
			cout << "|";
			for (int j = 0; j < 30; j++){
				if (space[downlines] - 1 == 0){
					drawline = true;
					space[downlines]--;
					downlines++;
				}else space[downlines]--;
				
				if (drawline == true && downlines != 5)cout << "|";
				else cout << "_";
				drawline = false;
			}
			cout << "|" << endl;
		}
	}
}

void DividedGraph::draw(vector<string> name, vector<int> power, bool vertical){
	float sum = 0;
	float space[5];
	for (int i = 0; i < 5; i++){
		sum += power[i];
	}
	
	if (vertical == true){
		
		for (int i = 0; i < 5; i++){
			space[i] = (power[i] / sum) * 10.0f; //Divide the data equally by ( power/total power * length )
			space[i] = roundf(space[i]);		 //Round the data, it wont fit with the float
		}
		system("cls");
		cout << "Vertical Divided Bar Graph" << endl;
		cout << "--------------------------------" << endl;
		cout << setw(3) << "Number" << setw(10) << "Hero Name" << setw(12) << "Hero Power" << setw(8) << "Space" << endl;
		for (int i = 0; i < 5; i++){
			cout << setw(3) << i+1 << setw(11) << name[i] << setw(12) << power[i] << setw(8) << space[i] << endl;
		}
		cout << "--------------------------------" << endl;
		cout << endl;
		cout << endl;
		int position = 0;
		bool drawLine = false;
		for (int i = 0; i < 11; i++) pointLocator(space, drawLine, position, i, true);	
	}else if (vertical == false){
		for (int i = 0; i < 5; i++){				//This only for display of dividing
			space[i] = (power[i] / sum) * 10.0f;	//Divide the data equally by ( power/total power * Height )
			space[i] = roundf(space[i]);			//Round the data, it wont fit with the float
		}
		system("cls");
		cout << "Horizontal Divided Bar Graph" << endl;
		cout << "--------------------------------" << endl;
		cout << setw(3) << "Number" << setw(10) << "Hero Name" << setw(12) << "Hero Power" << setw(8) << "Space" << endl;
		for (int i = 0; i < 5; i++){
			cout << setw(3) << i + 1 << setw(11) << name[i] << setw(12) << power[i] << setw(8) << space[i] << endl;
		}
		cout << "--------------------------------" << endl;
		cout << endl;
		cout << endl;
		for (int i = 0; i < 5; i++){			//This only for display of the bar (because the height is 30)
			space[i] = (power[i] / sum) * 30.0f;//Divide the data equally by ( power/total power * Height )
			space[i] = roundf(space[i]);
		}
		int position = 0;
		bool drawLine = false;
		for (int i = 0; i < 6; i++){
			pointLocator(space, drawLine, position, i, false);
		}
	}
}

ostream& operator<<(ostream &out, DividedGraph &cPoint){
	cPoint.draw(cPoint.class_name, cPoint.class_power, cPoint.class_vertical);
	return out;
}