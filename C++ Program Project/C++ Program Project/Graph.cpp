#include<iostream>
#include<iomanip>
#include<vector>
#include "Graph.h"


Graph::Graph()
{
}

Graph::Graph(vector<string> name, vector<int> power,bool vertical){
	for (int i = 0; i < name.size(); i++){
		class_name.push_back(name[i]);
	}
	for (int i = 0; i < power.size(); i++){
		class_power.push_back(power[i]);
	}
	class_vertical = vertical;
}

Graph::~Graph()
{
}

void Graph::pointLocator(vector<string> name, vector<int> power, int downlines, int index){
	for (int j = 0; j <= 50; j++){
		if (j % 9 == 0 && j != 0 && j <= 9){
			if (power[0] == index) cout << "^"; //Only print when it's meet the exact point
			else cout << " ";
			
		}else if (j > 9 && j % 8 == 1 && j < 48){
			if (power[downlines] == index) cout << "^"; //Only print when it's meet the exact point
			else cout << " ";
			
			downlines++;
		}else cout << " ";
		
	}
}

void Graph::draw(vector<string> name, vector<int> power, bool vertical){
	if (vertical == true){
		system("cls");
		cout << "Vertical Point Graph Default" << endl;
		cout << "--------------------------------" << endl;
		cout << endl;
		cout << endl;
		for (int i = 20; i >= 0; i--){
			int downline = 1;
			if (i % 5 == 0 && i != 0){  //Every 5 row it prints the number as indicator instead of |
				cout << setw(5) << i;
				pointLocator(name, power, downline, i);
				cout << endl;
			}else if (i != 0){
				cout << setw(5) << "|";
				pointLocator(name, power, downline, i);
				cout << endl;
			}else cout << setw(5) << "0";
			
		}
		for (int i = 0; i < 50; i++){
			cout << "-";
		}
		cout << endl;
		cout << "\t";
		for (int i = 0; i < 5; i++){
			cout << setw(8) << name[i] << setw(10);
		}
	}else if (vertical == false){
		system("cls");
		int downline = 0;
		cout << "Horizontal Point Graph Default" << endl;
		cout << "--------------------------------" << endl;
		cout << endl;
		cout << endl;
		for (int i = 10; i >= 0; i--){
			if (i != 0){
				if (i % 2 == 0){
					cout << setw(8) << name[downline] << "|";
					for (int j = 0; j < (power[downline] * 2); j++){
						cout << " ";
						if (j == 20 || j == 30 || j == 40) cout << "  ";  //Because the column expanded cout more when it reach 10/15/20 because the column will expand
						
					}
					cout << ">";
					downline++;
					cout << endl;
				}else cout << setw(9) << "|" << endl;
				
			}else cout << setw(9) << "0";
			
		}
		for (int i = 0; i <= 40; i++){
			if (i % 10 == 0 && i != 40 && i != 0) cout << i / 2;
			else if (i != 40) cout << "-";
			else cout << "20" << endl;
		}
	}
}

ostream& operator<<(ostream &out, Graph &cPoint){
	cPoint.draw(cPoint.class_name, cPoint.class_power, cPoint.class_vertical);
	return out;
}
