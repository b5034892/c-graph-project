#include<iostream>
#include<iomanip>
#include<vector>
#include "LineGraph.h"


LineGraph::LineGraph()
{
}

LineGraph::LineGraph(vector<string> name, vector<int> power, bool vertical){
	for (int i = 0; i < name.size(); i++) class_name.push_back(name[i]);
	for (int i = 0; i < power.size(); i++) class_power.push_back(power[i]);
	class_vertical = vertical;
}

LineGraph::~LineGraph()
{
}

void LineGraph::pointLocator(int downlines, int index, int posY[]){
	
	for (int j = 0; j <= 50; j++){
		
		if (j % 9 == 0 && j != 0 && j <= 9){
			if (posY[0] == index) cout << "^";
			else if (posY[0]>posY[1] && index < posY[0] && index >= posY[1]) cout << "|"; //If the 1st point is higher than the next point
			else if (posY[1]>posY[0] && index < posY[1] && index >= posY[0]) cout << "|"; //If the 1st point is lower than the next point
			else cout << " ";
		}else if (j > 9 && j % 8 == 1 && j < 48){
			if (posY[downlines] == index) cout << "^";
			else if (posY[downlines]>posY[downlines + 1] && index < posY[downlines] && index >= posY[downlines+1] && index != 1 && downlines != 4) cout << "|"; //If the current point is higher than the next point
			else if (posY[downlines + 1]>posY[downlines] && index < posY[downlines+1] && index >= posY[downlines]) cout << "|"; //If the current point is higher than the next point
			else cout << " ";
			
			downlines++;
		}else{
			if (downlines >= 0 && downlines <= 5){
				if (j < 9 && posY[0] == index) cout << "-"; //Draw a straight line
				else if (j > 9 && posY[downlines] == index) cout << "-"; //Draw a straight line
				else cout << " ";
			}
			
		}
	}
	cout << endl;
}

void LineGraph::draw(vector<string> name, vector<int> power, bool vertical){
	if (vertical == true){
		int posY[5]; 
		for (int i = 0; i < 5; i++){
			posY[i] = power[i]; //Save the Y into posY
		}
		system("cls");
		cout << "Vertical Line Graph" << endl;
		cout << "--------------------------------" << endl;
		cout << endl;
		cout << endl;
		for (int i = 20; i >= 0; i--){
			int downline = 1; //To track the current point
			if (i % 5 == 0 && i != 0){
				cout << setw(5) << i;
				pointLocator(downline, i,posY);
				
			}else if (i != 0){
				cout << setw(5) << "|";
				pointLocator(downline, i, posY);

			}else cout << setw(5) << "0";
			
		}
		for (int i = 0; i < 50; i++){
			cout << "-";
		}
		cout << endl;
		cout << "\t";
		for (int i = 0; i < 5; i++){
			cout << setw(8) << name[i] << setw(10);
		}
	}else if (vertical == false){
		int posX[5];
		for (int i = 0; i < 5; i++){
			posX[i] = power[i] * 2;
		}
		system("cls");
		int downline = 0;
		cout << "Horizontal Line Graph" << endl;
		cout << "--------------------------------" << endl;
		cout << endl;
		cout << endl;
		for (int i = 10; i >= 0; i--){
			if (i != 0){
				
				if (i % 2 == 0){ //This is for the 0,2,4,6,8 Row (Row with name)
					cout << setw(8) << name[downline] << "|";
					for (int j = 0; j < (power[downline] * 2); j++){
						
						if (j < posX[downline + 1] && i != 2){
							cout << " ";
							if (j == 20 || j == 30 || j == 40){
								cout << "  ";
							}
						}else if(i != 2){
							cout << "-";
							if (j == 20 || j == 30 || j == 40){
								cout << "--";
							}
						}else{
							cout << " ";
							if (j == 20 || j == 30 || j == 40){
								cout << "  ";
							}
						}
						
					}
					cout << ">";
					downline++;
					if (posX[downline - 1] < posX[downline]){
						for (int j = posX[downline - 1]; j < posX[downline]; j++){
							cout << "-";
							if (j == 20 || j == 30 || j == 40){
								cout << "--";
							}
						}
					}
					cout << endl;
				}else{ //This is for the 1,3,5,7,9 Row (Row without name)
					cout << setw(9) << "|";
					if (downline > 0 && downline < 5){
						for (int j = 0; j < (power[downline] * 2); j++){
							cout << " ";
							if (j == 20 || j == 30 || j == 40){
								cout << "  ";
							}
						}
					}
					
					if (i != 1){
						cout << "|";
					}
					
					cout << endl;
				}
			}else{
				cout << setw(9) << "0";
			}
		}
		for (int i = 0; i <= 40; i++){
			if (i % 10 == 0 && i != 40 && i != 0) cout << i / 2;
			else if (i != 40) cout << "-";
			else cout << "20" << endl;

		}
	}
}

ostream& operator<<(ostream &out, LineGraph &cPoint){
	cPoint.draw(cPoint.class_name, cPoint.class_power, cPoint.class_vertical);
	return out;
}