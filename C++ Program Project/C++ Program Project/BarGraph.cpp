#include "BarGraph.h"
#include <iostream>
#include <iomanip>
#include <vector>

BarGraph::BarGraph()
{
}

BarGraph::BarGraph(vector<string> name, vector<int> power, bool vertical){
	for (int i = 0; i < name.size(); i++) class_name.push_back(name[i]);
	for (int i = 0; i < power.size(); i++) class_power.push_back(power[i]);
	class_vertical = vertical;
}

BarGraph::~BarGraph()
{
}

void BarGraph::pointLocator(vector<string> name, vector<int> power, int downlines, int index){
	for (int j = 0; j <= 50; j++){
		if (j % 9 == 0 && j != 0 && j <= 9){
			if (power[0] >= index) cout << "^"; //Every time it meet the exact column cout ^
			else cout << " ";
			
		}else if (j > 9 && j % 8 == 1 && j < 48){
			if (power[downlines] >= index) cout << "^"; //Every time it meet the exact column cout ^
			else cout << " ";
			downlines++;
		}
		else cout << " ";

	}
}

void BarGraph::draw(vector<string> name, vector<int> power, bool vertical){
	if (vertical == true){
		system("cls");
		cout << "Vertical Bar Graph" << endl;
		cout << "--------------------------------" << endl;
		cout << endl;
		cout << endl;
		for (int i = 20; i >= 0; i--){
			int downline = 1;
			if (i % 5 == 0 && i != 0){ //Every 5 row it prints the number as indicator instead of |
				cout << setw(5) << i;
				pointLocator(name, power, downline, i);
				cout << endl;
			}else if (i != 0){
				cout << setw(5) << "|";
				pointLocator(name, power, downline, i);
				cout << endl;
			}else cout << setw(5) << "0";
		}
		for (int i = 0; i < 50; i++){ //Expand the column into 50 column
			if (i != 49){
				if (i % 9 == 0 && i != 0 && i <= 9)cout << "^"; //The first 9 column
				else if (i > 9 && i % 8 == 1 && i < 48) cout << "^"; //The rest is every 8 column
				else cout << "-";
			}else cout << "-" << endl;
		}
		cout << "\t";
		for (int i = 0; i < 5; i++){
			cout << setw(8) << name[i] << setw(10);
		}
	}else if (vertical == false){
		system("cls");
		int downline = 0;
		cout << "Horizontal Bar Graph" << endl;
		cout << "--------------------------------" << endl;
		cout << endl;
		cout << endl;
		for (int i = 10; i >= 0; i--){
			if (i != 0){
				if (i % 2 == 0){
					cout << setw(8) << name[downline] << "|";
					for (int j = 0; j < (power[downline] * 2); j++){ //Because the column expanded multiple the power
						cout << ">";
						if (j == 20 || j == 30 || j ==40)cout << ">>>"; //Because the column expanded cout more when it reach 10/15/20 because the column will expand
					}
					downline++;
					cout << endl;
				}else cout << setw(9) << "|" << endl;
			}else cout << setw(9) << "0";
		}
		for (int i = 0; i <= 40; i++){
			if (i % 10 == 0 && i != 40 && i != 0)cout << i / 2;
			else if (i != 40)cout << "-";
			else cout << "20" << endl;
		}
	}
}

ostream& operator<<(ostream &out, BarGraph &cPoint){
	cPoint.draw(cPoint.class_name, cPoint.class_power, cPoint.class_vertical);
	return out;
}
