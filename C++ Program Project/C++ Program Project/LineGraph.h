#pragma once
#include "Graph.h"
#include <vector>
class LineGraph :public Graph{
public:
	LineGraph();
	LineGraph(vector<string> name, vector<int> power, bool vertical);
	~LineGraph();

	void draw(vector<string> name, vector<int> power, bool vertical);
	void pointLocator(int downlines, int index, int posY[]);
	friend ostream& operator<<(ostream &out, LineGraph &cPoint);
private:
	vector<string> class_name;
	vector<int> class_power;
	bool class_vertical;
};

