//====================================================================================================================
//													C++ GRAPH PROJECT
//====================================================================================================================
//Name : Yoga Adyapratama
//Program Features :
//1.Showing the data in 4 different Graphs (Bar, Divided Bar, Line and Point)
//2.Input the content (Hero Name and Power) into a file
//3.Create your own Files with name of your own [Max 5 Files]
//4.Delete any Files you want
//5.Load a different File of your choice
//6.Sort data by Name and Power ! Avaiable in Ascending And Descending too !
//7.Fight and see which hero in your data is better ! Choose the fighting attribute you want !
//====================================================================================================================
//--For every pointLocator and Draw in .h Files--
//1.Point Locator is to create a check point by heroPowers
//2.Draw is to draw lines or bars or divider by heroPowers
//====================================================================================================================
#include <iostream>
#include <fstream>
#include <string>
#include <iomanip>
#include <stdio.h>	
#include <vector>
#include <algorithm>

#include "Graph.h"
#include "BarGraph.h"
#include "DividedGraph.h"
#include "LineGraph.h"

using namespace std;

ofstream outFile;		// For Writing The Heroes Data into File
ifstream inFile;		// For Reading The Heroes Data into File
ifstream loadNameFile;	// For Writing The Files Name into File
ofstream saveNameFile;	// For Reading The Files Name into File

//====================================================================================================================
//                                          TEMPLATE CLASS AND FUNCTION
//====================================================================================================================
template<class T>
class heroFight{
public:
	T attribute1;
	T attribute2;
};

template<class T>
int heroCompare(T hero1, T hero2){
	if (hero1 > hero2)return 1;
	else if (hero1 < hero2)return -1;
	else return 0;
}
//====================================================================================================================

//====================================================================================================================
//                                                FUNCTIONS PROTOTYPE
//====================================================================================================================

//Menus===========
int mainMenu(string currentfile);		//Main Menu
int horverMenu();						//Horizontal and vertical menu
int filesMenu(string currentfile);		//File option menu
int sortMenu(string currentfile);		//Sort menu
int heroFightMenu(string currentfile);	//Fight menu
//================

void fileContentToArray(vector<string>& name, vector<int>& power, string fileToLoad);	//Save the files content into array
void fileNameToArray(vector<string>& tempfile, int& count);								//Save the files name into array
void ArrayToFileName(vector<string> tempfile, int count, bool defaultFlag);				//Write the files name from array into file

void initialisation(vector<string> name, vector<int> power, vector<string>& tempfile, int& count, string& currentfile);	//Initialisation For Program
void pause(int numberVariable);																							//To hold the screen when the menu choice != 0

//Menu and sub Menu codes
void inputHeroSet(vector<string>& name, vector<int>& power, string currentfile);											 //Prototype of Input Hero Set Function
void showHeroSet(vector<string>& name, vector<int>& power, string currentfile);												 //Prototype of Show Hero Set Function
void fileNameChanger(int filecount, vector<string>& tempfile, string& currentfile);											 //Prototype of File Name Changer
void loadDifferentFile(vector<string> name, vector<int> power,int filecount, vector<string> tempfile, string& currentfile);	 //Prototype of Load Different Files
void createNewFileSub(int& filecount, vector<string>& tempfile, string& currentfile, int choice);							 //Prototype of Create New File Sub Function
void createNewFileMain(vector<string> name, vector<int> power,int& filecount, vector<string>& tempfile, string& currentfile);//Prototype of Create New File Main Function
void deleteAFile(int& filecount, vector<string>& tempfile, string& currentfile);											 //Prototype of Delete A File
void sortByName(vector<string>& name, vector<int>& power, string currentfile, bool ascending);								 //Prototype of Sort By Name
void sortByPower(vector<string>& name, vector<int>& power, string currentfile, bool ascending);								 //Prototype of Sort By Power
void fightOption1(vector<string> name, vector<int> power, string currentfile);												 //Prototype of Fight By Int
void fightOption2(vector<string> name, vector<int> power, string currentfile);												 //Prototype of Fight By Float
//================
//====================================================================================================================


//====================================================================================================================
//                                                 MAIN CODES
//====================================================================================================================
int main(){
	vector<string> heroName;	//Vector to store Heroes Name
	vector<string> tempFile;	//Vector to store Files Name
	vector<int> heroPower;		//Vector to store Hero Powers

	string currentFile;			//For current opened file
	int menu,					//For main menu choices
		subMenu,				//For each sub menu choices
		fileCount;				//For count how many files exist

	
	initialisation(heroName, heroPower, tempFile, fileCount, currentFile);	//Init the Heroe's name and power from files to Vector
	fileContentToArray(heroName, heroPower, tempFile[0]);					//Load the content of the default file to arrays

	do{
		menu = mainMenu(currentFile);
		if (menu == 1){ //Input new hero data and save it to current opened file
			inputHeroSet(heroName,heroPower,currentFile);
		}else if (menu == 2){
			showHeroSet(heroName, heroPower, currentFile); //Showing heroes data from current opened file
		}else if (menu == 3){
			do{
				subMenu = filesMenu(currentFile);
				if (subMenu == 1)fileNameChanger(fileCount, tempFile, currentFile);			//Change a name of a file of user choice
				else if (subMenu == 2)loadDifferentFile(heroName, heroPower,fileCount, tempFile, currentFile);	//Load a diffrent file of user choice
				else if (subMenu == 3)createNewFileMain(heroName, heroPower,fileCount, tempFile, currentFile);	//Create a new file [Max 5 files]
				else if (subMenu == 4)deleteAFile(fileCount, tempFile, currentFile);		//Delete a file of user choice
				pause(subMenu);
			} while (subMenu != 0);
		}
		else if (menu == 4){
			do{
				subMenu = sortMenu(currentFile);
				if (subMenu == 1)sortByName(heroName, heroPower, currentFile, true);		//Sort by Name Ascending
				else if (subMenu == 2)sortByPower(heroName, heroPower, currentFile, true);	//Sort by Power Ascending
				else if (subMenu == 3)sortByName(heroName, heroPower, currentFile, false);	//Sort by Name Descending
				else if (subMenu == 4)sortByPower(heroName, heroPower, currentFile, false);	//Sort by Power Descending
				pause(subMenu);
			} while (subMenu != 0);
		}
		else if (menu == 5){
			BarGraph pointVertical(heroName, heroPower, true);		
			BarGraph pointHorizontal(heroName, heroPower, false);	
			do{
				subMenu = horverMenu();
				if (subMenu == 1)cout << pointVertical;			//Print Bar Graph from current opened file Vertically
				else if (subMenu == 2)cout << pointHorizontal;	//Print Bar Graph from current opened file Horrizontally
				cout << endl;
				pause(subMenu);
			} while (subMenu != 0);
		}else if (menu == 6){
			Graph pointVertical(heroName,heroPower, true);
			Graph pointHorizontal(heroName, heroPower, false);
			do{
				subMenu = horverMenu();
				if (subMenu == 1)cout << pointVertical;			//Print Point Graph from current opened file Vertically
				else if (subMenu == 2)cout << pointHorizontal;	//Print Point Graph from current opened file Vertically
				cout << endl;
				pause(subMenu);
			} while (subMenu != 0);
		}else if (menu == 7){
			DividedGraph pointVertical(heroName, heroPower, true);
			DividedGraph pointHorizontal(heroName, heroPower, false);
			do{
				subMenu = horverMenu();
				if (subMenu == 1)cout << pointVertical;			//Print Divided Bar Graph from current opened file Vertically
				else if (subMenu == 2)cout << pointHorizontal;	//Print Divided Bar Graph from current opened file Vertically
				cout << endl;
				pause(subMenu);
			} while (subMenu != 0);
		}else if (menu == 8){
			LineGraph pointVertical(heroName, heroPower, true);
			LineGraph pointHorizontal(heroName, heroPower, false);
			do{
				subMenu = horverMenu();
				if (subMenu == 1)cout << pointVertical;			//Print Line Graph from current opened file Vertically
				else if (subMenu == 2)cout << pointHorizontal;	//Print Line Graph from current opened file Vertically
				cout << endl;
				pause(subMenu);
			} while (subMenu != 0);
		}
		else if (menu == 9){
			do{
				subMenu = heroFightMenu(currentFile);
				if (subMenu == 1)fightOption1(heroName, heroPower, currentFile);		//Fight by Int Value
				else if (subMenu == 2)fightOption2(heroName, heroPower, currentFile);	//Fight by float Value
			} while (subMenu != 0);
		}
	} while (menu != 0);
	outFile.close();
}
//====================================================================================================================


//====================================================================================================================
//                                                FUNCTIONS CODES
//====================================================================================================================
int mainMenu(string currentfile){
	system("cls");
	int chooseNumber;
	cout << "Welcome to Hero graph" << endl;
	cout << "--------------------------------" << endl;
	cout << "1. Input Hero Set" << endl;
	cout << "2. See Hero Set" << endl;
	cout << "3. File Option" << endl;
	cout << "4. Sort Current Loaded File" << endl;
	cout << "5. Bar Graph" << endl;
	cout << "6. Point Graph" << endl;
	cout << "7. Divided Bar Graph" << endl;
	cout << "8. Line Graph" << endl;
	cout << "9. Hero Fight" << endl;
	cout << "--------------------------------" << endl;
	cout << "Current Loaded File : " << currentfile << endl;
	cout << "Choose Menu [0 to exit] : ";
	cin >> chooseNumber;
	return chooseNumber;
}

int horverMenu(){
	system("cls");
	int chooseNumber;
	cout << "Graph Menu" << endl;
	cout << "--------------------------------" << endl;
	cout << "1. Vertical" << endl;
	cout << "2. Horizontal" << endl;
	cout << "--------------------------------" << endl;
	cout << "Choose Menu [0 to exit] : ";
	cin >> chooseNumber;
	return chooseNumber;
}

int filesMenu(string currentfile){
	system("cls");
	int chooseNumber;
	cout << "File Menu" << endl;
	cout << "--------------------------------" << endl;
	cout << "1. Change a File Name" << endl;
	cout << "2. Load Different Files" << endl;
	cout << "3. Create New File" << endl;
	cout << "4. Delete A File" << endl;
	cout << "--------------------------------" << endl;
	cout << "Current Loaded File : " << currentfile << endl;
	cout << "Choose Menu [0 to exit] : ";
	cin >> chooseNumber;
	return chooseNumber;
}

int sortMenu(string currentfile){
	system("cls");
	int chooseNumber;
	cout << "Sort Current Loaded File" << endl;
	cout << "--------------------------------" << endl;
	cout << "1. Sort by name [Ascending]" << endl;
	cout << "2. Sort by power [Ascending]" << endl;
	cout << "3. Sort by name [Descending]" << endl;
	cout << "4. Sort by power [Descending]" << endl;
	cout << "--------------------------------" << endl;
	cout << "Current Loaded File : " << currentfile << endl;
	cout << "Choose Menu [0 to exit] : ";
	cin >> chooseNumber;
	return chooseNumber;
}

int heroFightMenu(string currentfile){
	system("cls");
	int chooseNumber;
	cout << "Hero Fight Menu" << endl;
	cout << "--------------------------------" << endl;
	cout << "1. Fight by Strength, Attack Speed" << endl;
	cout << "2. Fight by Evasion, Weight" << endl;
	cout << "--------------------------------" << endl;
	cout << "Current Loaded File : " << currentfile << endl;
	cout << "Choose Menu [0 to exit] : ";
	cin >> chooseNumber;
	return chooseNumber;
}

void fileContentToArray(vector<string>& name, vector<int>& power, string fileToLoad){
	string tempName;
	int tempPower;
	name.clear();				//Clear the vector for different file
	power.clear();				//Clear the vector for different file
	inFile.open(fileToLoad);	//Open the file to read
	while (!inFile.eof()) {		//If it's not reach the end of files
		inFile >> tempName;
		name.push_back(tempName);
		inFile >> tempPower;
		power.push_back(tempPower);
	}
	inFile.close();				//Close the file
}

void fileNameToArray(vector<string>& tempfile, int& count){
	string tempName;
	count = 0;							//Reset the filesCount into 0
	loadNameFile.open("saveName.txt");	//Open the file to read the files name
	while (!loadNameFile.eof()){		//If it's not reach the end of files
		loadNameFile >> tempName;
		tempfile.push_back(tempName);
		count++;						//Count how much files exit
	}
	loadNameFile.close();				//Close the file
}

void ArrayToFileName(vector<string> tempfile, int count, bool defaultFlag){
	saveNameFile.open("saveName.txt");		//Open the file to write the files name
	for (int i = 0; i < count; i++){
		if (count == 1 && i == 0)saveNameFile << tempfile[i];
		else if (i == (count - 1))saveNameFile << tempfile[i];
		else saveNameFile << tempfile[i] << endl;
	}
	saveNameFile.close();					//Close the file
	if (defaultFlag == true){				//If the flag is on (Means create a new file) fill the file with default hero name and power
		outFile.open(tempfile[count - 1]);
		for (int i = 0; i < 5; i++){
			if (i == 4)outFile << "Default 0";
			else outFile << "Default 0" << endl;
		}
		outFile.close();					
	}
}

void initialisation(vector<string> name, vector<int> power, vector<string>& tempfile, int& count, string& currentfile){
	string tempName;
	fileNameToArray(tempfile, count);
	currentfile = tempfile[0]; //Set the 1st file in the save name file as the current opened file
}

void pause(int numberVariable){
	if (numberVariable != 0){
		system("pause");
	}
}

void inputHeroSet(vector<string>& name, vector<int>& power, string currentfile){
	system("cls");
	outFile.open(currentfile);
	name.clear();	//Clear the vector for new input
	power.clear();	//Clear the vector for new input
	string tempName;
	int tempPower;
	cout << "Input Hero Set" << endl;
	cout << "--------------------------------" << endl;
	for (int i = 1; i < 6; i++){										//Saving all the 5 new heroes data into files and into the Vector
		cout << "Input Hero name " << i << " : ";
		cin >> tempName;
		name.push_back(tempName);										//Save the name into Vector
		cout << "Input " << tempName << " power [1-20]: ";
		cin >> tempPower;
		power.push_back(tempPower);										//Save the power into vector
		if (i != 5)outFile << tempName << " " << tempPower << endl;		//Save the heroes data into file
		else if (i == 5)outFile << tempName << " " << tempPower;		//If it's the 5th data, saving without end line (To make the read file easier)
	}
}

void showHeroSet(vector<string>& name, vector<int>& power, string currentfile){
	system("cls");
	fileContentToArray(name, power, currentfile);
	cout << "Show Hero Set" << endl;
	cout << "--------------------------------" << endl;
	cout << "Hero Name" << setw(15) << "Hero Power" << endl;
	for (int i = 0; i < 5; i++){
		cout << setw(8) << name[i] << setw(12) << power[i] << endl;
	}
	system("pause");
}

void fileNameChanger(int filecount, vector<string>& tempfile, string& currentfile){
	system("cls");
	string tempName;
	int fileNumber;
	cout << "File Name Changer" << endl;
	cout << "--------------------------------" << endl;
	cout << "File Name List[Max 5 files]:" << endl;
	for (int i = 0; i < filecount; i++){
		cout << i + 1 << ". " << tempfile[i] << endl;	//Showing the list of the files exist
	}
	cout << "--------------------------------" << endl;
	
	do{
		cout << "Choose the file you want to change [1-" << filecount << "][0 to exit]: ";
		cin >> fileNumber;
	} while (fileNumber < 0 || fileNumber > filecount);
	if (fileNumber != 0){
		cout << "Insert your new file name [Auto extention]: ";
		cin >> tempName;
		tempName = tempName + ".txt"; //Automatically add the .txt extention
		for (int i = 0; i < filecount; i++){
			if (currentfile == tempfile[i]){
				currentfile = tempName;//change the current opened file name if it's currently loaded
			}
		}

		loadNameFile.close();//close all stream file before changing name
		saveNameFile.close();
		outFile.close();
		inFile.close();

		rename(tempfile[fileNumber - 1].c_str(), tempName.c_str());//rename the choosen files
		tempfile[fileNumber - 1] = tempName;
		ArrayToFileName(tempfile, filecount, false);
	}
}

void loadDifferentFile(vector<string> name, vector<int> power,int filecount, vector<string> tempfile, string& currentfile){
	system("cls");
	int loadChoice;
	cout << "Load Different File" << endl;
	cout << "--------------------------------" << endl;
	cout << "File Name List [Max 5 files]:" << endl;
	for (int i = 0; i < filecount; i++){
		cout << i + 1 << ". " << tempfile[i] << endl;	//Showing the list of the files exist
	}
	cout << endl;
	cout << "Current Loaded File : " << currentfile << endl;
	cout << "--------------------------------" << endl;
	do{
		cout << "Choose the file you want to load [1-" << filecount << "][0 to exit]: ";
		cin >> loadChoice;
	} while (loadChoice < 0 || loadChoice > filecount);
	if (loadChoice != 0){
		currentfile = tempfile[loadChoice - 1]; //Change the current file
		fileContentToArray(name, power, currentfile);
	}
}

void createNewFileSub(int& filecount, vector<string>& tempfile, string& currentfile, int choice){
	string tempName;
	bool newNameFlag = false;
	do{	
		newNameFlag = false;
		cout << "Insert your new file name [Auto extention]:";
		cin >> tempName;
		tempName = tempName + ".txt"; //Automatically add the .txt extention
		for (int i = 0; i < tempfile.size(); i++){
			if (tempfile[i] == tempName){
				newNameFlag = true; //Flag so user can't put the same name as the others file
			}
		}
	} while (newNameFlag == true || tempName == "");
	tempfile.push_back(tempName);
	filecount++;
	ArrayToFileName(tempfile, filecount, true);

	if (choice == 2){
		currentfile = tempfile[filecount - 1]; //Change the current file
		
	}
}

void createNewFileMain(vector<string> name, vector<int> power,int& filecount, vector<string>& tempfile, string& currentfile){
	system("cls");
	int createChoice;
	cout << "Create New File" << endl;
	cout << "--------------------------------" << endl;
	cout << "File Name List [Max 5 files]:" << endl;
	for (int i = 0; i < filecount; i++){
		cout << i + 1 << ". " << tempfile[i] << endl;
	}
	cout << endl;
	cout << "Total Files : " << filecount << " Files, you can create " << 5 - filecount << " more files." << endl;
	cout << "Current Loaded File : " << currentfile << endl;
	cout << "--------------------------------" << endl;
	if (filecount < 5){
		do{
			cout << "1.Create New File" << endl;
			cout << "2.Create and Load New Files" << endl;
			cout << "Choose Menu [0 to exit]: ";
			cin >> createChoice;
		} while (createChoice < 0 || createChoice > 2);
		cout << "--------------------------------" << endl;
		if (createChoice != 0){
			createNewFileSub(filecount, tempfile, currentfile, createChoice);
			fileContentToArray(name, power, currentfile);
		}
	}
	else if (filecount == 5){
		cout << "You can't create more files" << endl;
	}
}

void deleteAFile(int& filecount, vector<string>& tempfile, string& currentfile){
	system("cls");
	int loadChoice;
	cout << "Delete A File" << endl;
	cout << "--------------------------------" << endl;
	cout << "File Name List [Max 5 files]:" << endl;
	for (int i = 0; i < filecount; i++){
		cout << i + 1 << ". " << tempfile[i] << endl;
	}
	cout << endl;
	cout << "Current Loaded File : " << currentfile << endl;
	cout << "--------------------------------" << endl;
	do{
		cout << "Choose the file you want to delete [1-" << filecount << "][0 to exit]: ";
		cin >> loadChoice;
	} while (loadChoice < 0 || loadChoice > filecount);
	if (loadChoice != 0){
		remove(tempfile[loadChoice - 1].c_str());			//Remove the file used choosed (File will automatically deleted)
		tempfile.erase(tempfile.begin()+(loadChoice - 1));	//Remove the choosed file from the vector
		filecount--;										//Reduce the file count
		ArrayToFileName(tempfile, filecount, false);
	}
}


void sortByName(vector<string>& name, vector<int>& power, string currentfile, bool ascending){
	vector<int> index(name.size(), 0);						//Vector for index
	for (int i = 0; i < index.size(); i++)index[i] = i;		//Assign index to remember the original position

	if (ascending == true)sort(index.begin(), index.end(), [&](const int& a, const int& b) {		 //Ascending
		return (name[a] < name[b]);
	});
	else if (ascending == false)sort(index.rbegin(), index.rend(), [&](const int& a, const int& b) { //Descending
		return (name[a] < name[b]);
	});
	outFile.open(currentfile);
	for (int i = 0; i < 5; i++){		//Write the sorted data into file
		if (i != 4)outFile << name[index[i]] << " " << power[index[i]] << endl;
		else if (i == 4)outFile << name[index[i]] << " " << power[index[i]];
	}
	outFile.close();
	fileContentToArray(name, power, currentfile);
}

void sortByPower(vector<string>& name, vector<int>& power, string currentfile, bool ascending){
	vector<int> index(power.size(), 0);						//Vector for index
	for (int i = 0; i < index.size(); i++)index[i] = i;		//Assign index to remember the original position
	
	if (ascending == true)sort(index.begin(), index.end(), [&](const int& a, const int& b) {		//Ascending
		return (power[a] < power[b]);
	});
	else if (ascending == false)sort(index.rbegin(), index.rend(), [&](const int& a, const int& b) {//Descending
		return (power[a] < power[b]);
	});
	outFile.open(currentfile);
	for (int i = 0; i < 5; i++){		//Write the sorted data into file
		if (i != 4)outFile << name[index[i]] << " " << power[index[i]] << endl;
		else if (i == 4)outFile << name[index[i]] << " " << power[index[i]];
	}
	outFile.close();
	fileContentToArray(name, power, currentfile);
}

void fightSubMenu(int& heroChoice1, int& heroChoice2, vector<string> name, vector<int> power, string currentfile){
	fileContentToArray(name, power, currentfile);
	cout << "Hero Lists" << endl;
	cout << "--------------------------------" << endl;
	cout << "Number" << setw(12) << "Hero Name" << setw(15) << "Hero Power" << endl;
	for (int i = 0; i < 5; i++){
		cout << setw(3) << i + 1 << setw(12) << name[i] << setw(15) << power[i] << endl;
	}
	cout << "--------------------------------" << endl;
	do{
		cout << "Choose first hero you want to use [1-5]: ";
		cin >> heroChoice1;
	} while (heroChoice1 < 0 || heroChoice1 > 5);
	do{
		cout << "Choose second hero you want to use [1-5][You can't use same hero]: ";
		cin >> heroChoice2;
	} while (heroChoice2 < 0 || heroChoice2 > 5 || heroChoice2 == heroChoice1);
	cout << "--------------------------------" << endl;
}

void fightOption1(vector<string> name, vector<int> power, string currentfile){
	int heroChoice1, heroChoice2, status1, status2;
	heroFight<int> hero1, hero2; //Using the template class into INT
	fightSubMenu(heroChoice1, heroChoice2, name, power, currentfile);
	hero1.attribute1 = power[heroChoice1 - 1] * 3;
	hero2.attribute1 = power[heroChoice2 - 1] * 3;

	hero1.attribute2 = power[heroChoice1 - 1] * 2;
	hero2.attribute2 = power[heroChoice2 - 1] * 2;

	status1 = heroCompare(hero1.attribute1, hero2.attribute1);	//Using the template function as INT
	status2 = heroCompare(hero1.attribute1, hero2.attribute1);	//Using the template function as INT

	//Print
	system("cls");
	cout << "Fight by Strength, Attack Speed" << endl;
	cout << "--------------------------------" << endl;
	cout << "Hero Name" << setw(12) << "Strength" << setw(15) << "Attack Speed" << endl;
	cout << name[heroChoice1 - 1] << setw(12) << hero1.attribute1 << setw(15) << hero1.attribute2 << endl;
	cout << name[heroChoice2 - 1] << setw(12) << hero2.attribute1 << setw(15) << hero2.attribute2 << endl;
	cout << "--------------------------------" << endl;
	if (status1 == 1)cout << name[heroChoice1 - 1] << "'s Strength better than " << name[heroChoice2 - 1] << "'s" << endl;
	else if (status1 == -1)cout << name[heroChoice2 - 1] << "'s Strength better than " << name[heroChoice1 - 1] << "'s" << endl;
	else if(status1 == 1)cout << name[heroChoice1 - 1] << "'s Strength equal to " << name[heroChoice2 - 1] << "'s" << endl;

	if (status2 == 1)cout << name[heroChoice1 - 1] << "'s Attack Speed better than " << name[heroChoice2 - 1] << "'s" << endl;
	else if (status2 == -1)cout << name[heroChoice2 - 1] << "'s Attack Speed better than " << name[heroChoice1 - 1] << "'s" << endl;
	else if (status2 == 1)cout << name[heroChoice1 - 1] << "'s Attack Speed equal to " << name[heroChoice2 - 1] << "'s" << endl;
	system("pause");
}

void fightOption2(vector<string> name, vector<int> power, string currentfile){
	int heroChoice1, heroChoice2;
	float status1, status2;
	heroFight<float> hero1, hero2; //Using the template class into FLOAT
	fightSubMenu(heroChoice1, heroChoice2, name, power, currentfile);
	hero1.attribute1 = power[heroChoice1 - 1] * 1.5f;
	hero2.attribute1 = power[heroChoice2 - 1] * 1.5f;

	hero1.attribute2 = power[heroChoice1 - 1] * 2.5f;
	hero2.attribute2 = power[heroChoice2 - 1] * 2.5f;

	status1 = heroCompare(hero1.attribute1, hero2.attribute1);	//Using the template function as FLOAT
	status2 = heroCompare(hero1.attribute1, hero2.attribute1);	//Using the template function as FLOAT

	//Print
	system("cls");
	cout << "Fight by Evasion, Weight" << endl;
	cout << "--------------------------------" << endl;
	cout << "Hero Name" << setw(12) << "Evasion" << setw(15) << "Weight" << endl;
	cout << name[heroChoice1 - 1] << setw(15) << hero1.attribute1 << setw(18) << hero1.attribute2 << endl;
	cout << name[heroChoice2 - 1] << setw(15) << hero2.attribute1 << setw(18) << hero2.attribute2 << endl;
	cout << "--------------------------------" << endl;
	if (status1 == 1)cout << name[heroChoice1 - 1] << "'s Evasion better than " << name[heroChoice2 - 1] << "'s" << endl;
	else if (status1 == -1)cout << name[heroChoice2 - 1] << "'s Evasion better than " << name[heroChoice1 - 1] << "'s" << endl;
	else if (status1 == 1)cout << name[heroChoice1 - 1] << "'s Evasion equal to " << name[heroChoice2 - 1] << "'s" << endl;

	if (status2 == 1)cout << name[heroChoice1 - 1] << "'s Weight better than " << name[heroChoice2 - 1] << "'s" << endl;
	else if (status2 == -1)cout << name[heroChoice2 - 1] << "'s Weight better than " << name[heroChoice1 - 1] << "'s" << endl;
	else if (status2 == 1)cout << name[heroChoice1 - 1] << "'s Weight equal to " << name[heroChoice2 - 1] << "'s" << endl;
	system("pause");
}
//====================================================================================================================