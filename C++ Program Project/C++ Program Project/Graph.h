#include <string>
#include <vector>
#pragma once

using namespace std;

class Graph{
public:
	Graph();
	Graph(vector<string> name, vector<int> power, bool vertical);
	~Graph();

	virtual void draw(vector<string> name, vector<int> power, bool vertical);
	virtual void pointLocator(vector<string> name, vector<int> power, int downlines, int index);
	friend ostream& operator<<(ostream &out, Graph &cPoint);
private:
	vector<string> class_name;
	vector<int> class_power;
	bool class_vertical;
};

