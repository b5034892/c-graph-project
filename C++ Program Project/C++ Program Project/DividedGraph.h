#pragma once
#include "Graph.h"
#include <vector>
class DividedGraph :public Graph{
public:
	DividedGraph();
	DividedGraph(vector<string> name, vector<int> power, bool vertical);
	~DividedGraph();

	void draw(vector<string> name, vector<int> power, bool vertical);
	void pointLocator(float space[], bool drawline, int& downlines, int index, bool vertical);
	friend  ostream& operator<<(ostream &out, DividedGraph &cPoint);
private:
	vector<string> class_name;
	vector<int> class_power;
	bool class_vertical;
};

